<?php
/*
 * uniform offence names
 * @Desc Offence::NRR;
 * */
namespace app;

abstract class Offence
{
	const RDM = 'RDM';
	const NRR = 'NRR';
	const iHelp = 'Illegal Help';
	const iHalt = 'Illegal Halt';
	const iDemand = 'Illegal Demand';
	const iWar = 'Illegal War';
	const Glitch = 'Glitching';
	const Loop = 'Loopholing';
	const CLog = 'Combat log';
}

?>
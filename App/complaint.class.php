<?php

namespace app\core;

require_once 'app.config.php';
require_once 'offence.enum.php';
class Complaint
{
	private $forumID;
	private $author;
	private $title;
	private $post;
	public final function __construct(int $author = 0, string $title = "none", string $post = "<p>Trace @ stdClass Complaint::__construct() -> create()</p>")
	{
		$this->forumID = \app\config\FORUM_COMPLAINTS_EU;
		$this->author = $author;
		$this->title = $title;
		$this->post = $post;
	}
	// mixed $offence -> multiple or one ofence report
	//TODO shift into create function
	public final function setup(string $accuser, string $accused, $offence = null)
	{
	}
	public final function create(array $optional = null)/*: string*/
		{
		$data = array (
				'forum' => $this->forumID,
				'author' => $this->author,
				'title' => $this->title,
				'post' => $this->post 
		);
		
		if (isset ( $optional ))
		{
			// Add optional vars to the Topic
			$data = array_merge ( $data, $optional );
		}
		var_dump ( $data );
		
		$curl = curl_init ( \app\config\URL . \app\config\TOPICS );
		curl_setopt_array ( $curl, array (
				CURLOPT_RETURNTRANSFER => TRUE,
				CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
				CURLOPT_USERPWD => \app\config\API_KEY . ":",
				CURLOPT_POSTFIELDS => $data 
		) );
		return curl_exec ( $curl );
	}
}

?>